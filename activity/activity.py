# Create 5 variables and output them in the command prompt in the following format:
# a. "I am < name (string)> , and I am < age (integer)> years old, I work as a < occupation 
#    (string)> , and my rating for < movie (string)> is < rating (decimal)> %"

name = str("Gerald Glenn Silverio")
age = int(23)
occupation = str("student")
movie = str("Whiplash")
rating = float("93")

print(f"I am {name} and I am {age} years old, I work as a {occupation}, and my rating for {movie} is {rating}%")

# Create 3 variables, num1, num2, and num3
# a. Get the product of num1 and num2
# b. Check if num1 is less than num3
# c. Add the value of num3 to num2

num1 = 32
num2 = 8
num3 = 22

print(num1 * num2)

if num1 < num3:
    print("num1 is LESS THAN num3")
else:
    print("num1 is NOT LESS THAN num3")

num2 += num3

print(num2)